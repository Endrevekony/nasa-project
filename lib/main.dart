import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nasa_app/features/apod/presentation/pages/master_page.dart';
import 'features/apod/presentation/bloc/apod_bloc.dart';
import 'injection.container.dart';
import 'injection.container.dart' as di;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  runApp(NasaApp());
}

class NasaApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Nasa application'),
        ),
        body: SafeArea(
          child: MultiBlocProvider(
            providers: [
              BlocProvider<ApodBloc>(
                create: (_) => sl<ApodBloc>(),
              ),
            ],
            child: const MasterPage(),
          ),
        ),
      ),
    );
  }
}
