import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:nasa_app/features/apod/data/data_sources/apod_remote_data_source.dart';
import 'package:nasa_app/features/apod/data/repositories/apod_repository_impl.dart';
import 'package:nasa_app/features/apod/domain/repositories/apod_repository.dart';
import 'package:nasa_app/features/apod/domain/use_cases/get_apods_use_case.dart';
import 'package:nasa_app/features/apod/presentation/bloc/apod_bloc.dart';
import 'core/network_info.dart';
import 'package:http/http.dart' as http;
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //bloc
  sl.registerLazySingleton(() => ApodBloc(getApods: sl()));

  //usecase
  sl.registerLazySingleton(() => GetApodsUseCase(sl()));

  //repository
  sl.registerLazySingleton<ApodRepository>(
    () => ApodRepositoryImpl(remoteDataSource: sl(), networkInfo: sl()),
  );
  
  //data source
  sl.registerLazySingleton<ApodRemoteDataSource>(
        () => ApodRemoteDataSourceImpl( client: sl()),
  );


  //network
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  
  //external
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
