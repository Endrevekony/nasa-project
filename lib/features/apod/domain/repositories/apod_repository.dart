import 'package:dfunc/dfunc.dart';
import 'package:nasa_app/core/failures.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';

abstract class ApodRepository {
  Future<Either<Failure, ApodDataModel>> getApods();
}