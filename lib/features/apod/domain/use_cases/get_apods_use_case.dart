
import 'package:dfunc/dfunc.dart';
import 'package:nasa_app/core/failures.dart';
import 'package:nasa_app/core/usecase.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';
import 'package:nasa_app/features/apod/domain/repositories/apod_repository.dart';

class GetApodsUseCase implements UseCase<ApodDataModel, NoParams> {
  final ApodRepository repository;

  GetApodsUseCase(this.repository);

  @override
  Future<Either<Failure, ApodDataModel>> call(NoParams params) async {
    return await repository.getApods();
  }
}
