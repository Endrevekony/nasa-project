import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';
import 'package:nasa_app/features/apod/presentation/pages/detail_page.dart';
import 'package:nasa_app/features/apod/presentation/widgets/video_widget.dart';

class MasterPageWidget extends StatelessWidget {
  final ApodDataModel model;

  const MasterPageWidget({required this.model});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: Text(
              model.title,
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        (model.mediaType == "image")
            ? Padding(
                padding: const EdgeInsets.all(10),
                child: Center(
                  child: CachedNetworkImage(
                    imageUrl: model.url,
                  ),
                ),
              )
            : Center(
                child: VideoWidget(
                  url: model.url,
                ),
              ),
        const SizedBox(
          height: 15,
        ),
        Center(
          child: FloatingActionButton.extended(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => DetailsPage(
                    model: model,
                  ),
                ),
              );
            },
            label: const Text("Go to details page"),
          ),
        )
      ],
    );
  }
}
