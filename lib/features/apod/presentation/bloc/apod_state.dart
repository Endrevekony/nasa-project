part of 'apod_bloc.dart';


abstract class ApodState extends Equatable {
  @override
  List<Object> get props => [];
}

class NoApodData extends ApodState {}

class GotResult extends ApodState {
  final ApodDataModel result;

  GotResult({required this.result});

  @override
  List<Object> get props => [result];
}

class Loading extends ApodState {}

class ApodError extends ApodState {
  final String error;

  ApodError({required this.error});

  @override
  List<Object> get props => [error];
}


