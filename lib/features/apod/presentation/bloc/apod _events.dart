part of 'apod_bloc.dart';

abstract class ApodEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetApods extends ApodEvent {}

