import 'package:dfunc/dfunc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nasa_app/core/failures.dart';
import 'package:nasa_app/core/usecase.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';
import 'package:nasa_app/features/apod/domain/use_cases/get_apods_use_case.dart';

part 'apod _events.dart';
part 'apod_state.dart';

class ApodBloc extends Bloc<ApodEvent, ApodState> {
  final GetApodsUseCase getApods;
  ApodBloc({required this.getApods}) : super(NoApodData());

  @override
  Stream<ApodState> mapEventToState(ApodEvent event) async* {
    if (event is GetApods) {
      yield Loading();
      final result = await getApods(NoParams());
      yield* _eitherApodOrErrorState(result);
    }
  }

  Stream<ApodState> _eitherApodOrErrorState(
    Either<Failure, ApodDataModel> either,
  ) async* {
    yield either.fold(
      (failure) => ApodError(error: "server problem happened"),
      (apod) => GotResult(result: apod),
    );
  }
}
