import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nasa_app/features/apod/presentation/bloc/apod_bloc.dart';
import 'package:nasa_app/features/apod/presentation/widgets/master_page_widget.dart';

import '../../../../injection.container.dart';

class MasterPage extends StatelessWidget {
  const MasterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: BlocBuilder<ApodBloc, ApodState>(
          bloc: sl<ApodBloc>(),
          builder: (context, state) {
            if (state is Loading) {
              return const Center(child: CircularProgressIndicator.adaptive());
            }
            if (state is NoApodData) {
              sl<ApodBloc>().add(GetApods());
              return const Center(child: CircularProgressIndicator.adaptive());
            }
            if (state is GotResult) {
              return MasterPageWidget(
                model: state.result,
              );
            }
            if (state is ApodError) {
              return Center(child: Text(state.error));
            }
            return const CircularProgressIndicator.adaptive();
          }),
    );
  }
}
