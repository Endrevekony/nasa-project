import 'package:flutter/material.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';

class DetailsPage extends StatelessWidget {
  final ApodDataModel model;

  const DetailsPage({required this.model});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Details page'),
      ),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Center(
                child: Text(
              "Dátum: ${model.date}",
              style: const TextStyle(fontWeight: FontWeight.bold),
            )),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Center(child: Text(model.explanation)),
          )
        ],
      )),
    );
  }
}
