import 'package:dfunc/dfunc.dart';
import 'package:nasa_app/core/failures.dart';
import 'package:nasa_app/core/network_info.dart';
import 'package:nasa_app/features/apod/data/data_sources/apod_remote_data_source.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';
import 'package:nasa_app/features/apod/domain/repositories/apod_repository.dart';

class ApodRepositoryImpl extends ApodRepository {
  final ApodRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  ApodRepositoryImpl({required this.remoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, ApodDataModel>> getApods() async{
    if(await networkInfo.isConnected) {
      final apods = await remoteDataSource.getAllEvents();
      return Either.right(apods);
    }
    return Either.left(ServerFailure());
  }

}