import 'package:http/http.dart' as http;
import 'package:nasa_app/core/failures.dart';
import 'package:nasa_app/features/apod/data/models/apod_data_model.dart';

abstract class ApodRemoteDataSource {
  Future<ApodDataModel> getAllEvents();
}

class ApodRemoteDataSourceImpl extends ApodRemoteDataSource {
  final http.Client client;

  ApodRemoteDataSourceImpl({required this.client});

  @override
  Future<ApodDataModel> getAllEvents() async {
    Uri url = Uri.parse("https://api.nasa.gov/planetary/apod?api_key=o0UtQ0CY7Zq2JLTXyO0ptUMQ3o5E3nsYf6zx4aBu");
    final response = await client.get(url,headers: {'Content-Type': 'application/json'},);
    if(response.statusCode == 200) {
      final ApodDataModel model = apodDataModelFromJson(response.body);
      return model;
    }
    return throw ServerFailure();
  }
}
